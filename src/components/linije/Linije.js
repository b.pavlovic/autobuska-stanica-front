import React from 'react';
import { Form,Button, Table, Row, Col, FormGroup } from 'react-bootstrap';
import DevelopingAxios from '../../apis/DevelopingAxios';

class Linije extends React.Component{

    constructor(props)
    {
        super(props);

        const newLinija={
            brojMesta: 0,
            cenaKarte: 0,
            destinacija:"",
            vremePolaska:"",
            prevoznikDTO:{
                id: -1
            }
        }

        const search ={
            destinacija: "",
            prevoznikId: 0,
            cenaKarte:0
        }

        const reserve={
            brojMesta:0,
            linijaDTO:{
                id:-1
            }
        }

        this.state = { linije : [],
                       prevoznici:[], 
                       pageNo: 0,
                       totalPages: 0 ,
                       newLinija: newLinija,
                       Check: false, 
                       search: search,
                       reserve: reserve 
                    }
    }

    componentDidMount(){
        this.getLinije(this.state.pageNo);
        this.getPrevoznici()
    }

    getPrevoznici(){
        DevelopingAxios.get('/prevoznici')
            .then(res=>{
                this.setState({
                    prevoznici: res.data
                })
            })
            .catch(error=>{
                console.log(error)
            })
    }

    getLinije(newPageNo){
      const config={
           params:{
               pageNo: newPageNo,
           }
       }

       if(this.state.search.destinacija!==""){
        config.params['destinacija']=this.state.search.destinacija
    }
    if(this.state.search.prevoznikId!==0){
        config.params['prevoznikId']= this.state.search.prevoznikId
    }
    if(this.state.search.cenaKarte!==0){
        config.params['cenaKarte']= this.state.search.cenaKarte
    }


        DevelopingAxios.get('/linije', config)
            .then(res=>{
                this.setState({
                    linije: res.data,
                    totalPages: res.headers['total-pages'],
                    pageNo: newPageNo
                })
            })
            .catch(error=>{
                console.log(error);
            })
    }

    postLinija(){
            var params = {
                "destinacija": this.state.newLinija.destinacija,
                "vremePolaska": this.state.newLinija.vremePolaska,
                "cenaKarte": this.state.newLinija.cenaKarte,
                "brojMesta": this.state.newLinija.brojMesta,
                "prevoznikDTO":{
                    "id": this.state.newLinija.prevoznikDTO.id
                }
            }  

        DevelopingAxios.post('/linije/', params)
            .then(res=>{
                console.log(res.data)
                this.myMethod()
            })
            .catch(error=>{
                console.log(error);
            })
    }

    setovanjeLinija(data){
        this.setState({
            linije: data
        })
    }

    myMethod(){
        this.getLinije(this.state.totalPages-1)
    }

    deleteLinija(id){
        DevelopingAxios.delete('/linije/' + id)
            .then(res=>{
                console.log(res)
                this.myMethod()
            })
            .catch(error=>{
                console.log(error);
            })

    }

    goToEdit(id){
        this.props.history.push('/linije/edit/'+ id);
    }

    rezervisanje(linijaId, destinacija, vremePolaska){
        this.setState({
            brojMesta:1,
            linijaDTO:{
                id: linijaId
            }
        })
        var params={
                brojMesta: 1,
                 linijaDTO:{
                     id: linijaId
                     }
        }   
        DevelopingAxios.post('/rezervacija/', params)
            .then(res=>{
                console.log(res);
                alert("Uspesno ste rezervisali kartu za polazak za " + destinacija + " u " + vremePolaska + " casova")
                this.myMethod()
            })
            .catch(error=>{
                console.log(error);
            })
    }

    renderLinije(){
        return this.state.linije.map(linija=>{
            return (
                <tr key={linija.id}>
                    <td>{linija.destinacija}</td>
                    <td>{linija.vremePolaska}</td>
                    <td>{linija.cenaKarte}</td>
                    <td>{linija.brojMesta}</td>
                    <td>{linija.prevoznikDTO.naziv}</td>
                    {window.localStorage['role']==='ROLE_ADMIN'?[
                    <td><Button variant="primary" onClick={()=>this.rezervisanje(linija.id, linija.destinacija, linija.vremePolaska)}>Reserve</Button></td>,
                    <td><Button variant="warning" onClick={()=>this.goToEdit(linija.id)}>Edit</Button></td>,
                    <td><Button variant="danger" onClick={()=>this.deleteLinija(linija.id)}>Delete</Button></td>]:null}
                </tr>
            )
        })
    }

    onInputChange(e){
        const name = e.target.name;
        const value = e.target.value;
        let newLinija = JSON.parse(JSON.stringify(this.state.newLinija))
        newLinija[name]= value;
        this.setState({newLinija:newLinija})
    }

    onSearchInputChange(e){
        const name = e.target.name;
        const value = e.target.value;
        let search = JSON.parse(JSON.stringify(this.state.search))
        search[name]= value;
        this.setState({search:search})
    }

    onSelectPrevozniciSearch(e){
        const value = e.target.value;
        console.log(value)
        let search = this.state.search;
        search.prevoznikId = value;
        this.setState({
            search:search
        })
    }

    onSelectPrevoznikChange(e){
        // const name = e.target.name;
        const value = e.target.value;
        console.log(value)
        let newLinija = this.state.newLinija;
        newLinija.prevoznikDTO.id = value;
        this.setState({
            newLinija:newLinija
        })
    }

    inputChecked(e){
        const value = e.target.checked
        this.setState({
            Check: value
        })
    }

    render(){
        return(
            <div>
                <Form>
                <Form.Group>
                     <Row><Col>
                    <Button onClick={()=>this.postLinija()}>Add</Button>
                    </Col></Row>
                </Form.Group>
                <Form.Group>
                    <Row><Col>
                    <Form.Label>Unesi destinaciju</Form.Label>
                    <Form.Control 
                    value={this.state.newLinija.destinacija}
                    name="destinacija"
                    as="input" 
                    type="text"
                    onChange={(e)=>this.onInputChange(e)}>
                    
                    </Form.Control>
                    </Col></Row>
                </Form.Group> 
                <Form.Group> 
                    <Row><Col>  
                    <Form.Label>Unesi vreme polaska</Form.Label>
                    <Form.Control 
                    value={this.state.newLinija.vremePolaska}
                     name="vremePolaska"
                     as="input" 
                     type="text"
                     onChange={(e)=>this.onInputChange(e)}>
                     </Form.Control>
                    </Col></Row>
                </Form.Group>
                <Row>
                <Col> 
                 
                    <Form.Label>Unesi cenu karte</Form.Label>
                    <Form.Control 
                    value={this.state.newLinija.cenaKarte}
                    name="cenaKarte"
                    as="input" 
                    type="number"
                    onChange={(e)=>this.onInputChange(e)}>
                    </Form.Control>
                <Form.Group>
                </Form.Group>  
                </Col>  
                <Col> 
                <Form.Group>
                    <Form.Label>Broj slobodnih mesta</Form.Label>
                    <Form.Control 
                    value={this.state.newLinija.brojMesta}
                    name="brojMesta"
                    as="input" 
                    type="number"
                    onChange={(e)=>this.onInputChange(e)}>
                    </Form.Control> 
                </Form.Group>
                </Col>
                </Row>
                <Row>
                    <Col>
                    <Form.Group>
                        <Form.Control
                        value={this.state.newLinija.prevoznikDTO.id}
                        name="prevoznikId"
                        as="select"
                        onChange={(e)=>this.onSelectPrevoznikChange(e)} 
                        >
                            <option value={-1}>Izaberi prevoznika</option>
                            {this.state.prevoznici.map(prevoznik=>{
                                return(
                                    <option value={prevoznik.id} key={prevoznik.id}>{prevoznik.naziv}</option>
                                );
                            })}
                        </Form.Control>
                    </Form.Group>
                    </Col>
                </Row>
                </Form>
                <Form>
                <Form.Group>
                <Row>
                 <Col>
                    <Form.Check
                    name="Check"
                    type="checkbox" 
                    label="Check for search"
                    checked={this.state.Check}
                    onChange={(e)=>this.inputChecked(e)}
                    />
                    <Form.Group hidden={!this.state.Check}>
                            <Form.Label>Destinacija</Form.Label>
                            <Form.Control
                            value={this.state.search.destinacija}
                            name="destinacija"
                            type="text"
                            onChange={(e)=>this.onSearchInputChange(e)}></Form.Control>
                             <Form.Label>Max cena</Form.Label>
                            <Form.Control
                            value={this.state.search.cenaKarte}
                            name="cenaKarte"
                            type="number"
                            onChange={(e)=>this.onSearchInputChange(e)}
                            ></Form.Control>
                             <Form.Label
                            
                             ></Form.Label>
                            <Form.Control
                            value={this.state.search.prevoznikId}
                             name="prevoznikDTO"
                             as="select"
                             onChange={(e)=>this.onSelectPrevozniciSearch(e)}
                            >
                                <option value={0}>Izaberi prevoznika</option>
                                {this.state.prevoznici.map(prevoznik=>{
                                    return(
                                        <option key={prevoznik.id} value={prevoznik.id}> {prevoznik.naziv}</option>
                                    );
                                }
                                )}

                            </Form.Control>
                            <Button onClick={()=>this.getLinije(this.state.pageNo)}>Search</Button>
                    </Form.Group>
                    </Col>
                </Row>
                </Form.Group>
                </Form>
                <h1>Autobuske linije</h1>
                <Table style={{marginTop:5}}>
                    <thead>
                        <tr>
                            <th>Destinacija</th>
                            <th>Vreme polaska</th>
                            <th>Cena karte</th>
                            <th>Broj mesta</th>
                            <th>Prevoznik</th>
                            {window.localStorage['role']==='ROLE_ADMIN'?[
                            <th>Action</th>,
                            <th>Action</th>,
                            <th>Action</th>]:null}
                         </tr>
                     </thead>
                    <tbody>
                        {this.renderLinije()}
                     </tbody>
                </Table>
                <Form.Group >
                    <Button disabled={this.state.pageNo==0} onClick={()=>this.getLinije(this.state.pageNo-1)}>Prev</Button>
                    <Button disabled={this.state.pageNo +1 >= this.state.totalPages} onClick={()=>this.getLinije(this.state.pageNo+1)}>Next</Button>
                </Form.Group>
            </div>
        );
    }
}

export default Linije;