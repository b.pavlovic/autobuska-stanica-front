import React from 'react';
import { Form,Button, Table, Row, Col, FormGroup } from 'react-bootstrap';
import DevelopingAxios from '../../apis/DevelopingAxios';

class EditLinija extends React.Component {
    
    constructor(props){
        super(props);

        const newLinija={
            id:-1,
            destinacija:"",
            vremePolaska:"",
            brojMesta:-1,
            cenaKarte:0,
            prevoznikDTO:{
                id:0
            }
        }

        this.state={
            newLinija: newLinija,
            prevoznici:[]
        }
    }

    componentDidMount(){
        this.getPrevoznici();
        this.getLinijaById(this.props.match.params.id);

    }

    getLinijaById(id){
        DevelopingAxios.get('/linije/'+ id)
            .then(res=>{
                console.log(res.data);
                this.setState({
                    newLinija: res.data
                })
            })
            .catch(error=>{
                console.log(error)
            })
    }

    getPrevoznici(){
        DevelopingAxios.get('/prevoznici')
            .then(res=>{
                console.log(res.data)
                this.setState({
                    prevoznici: res.data
                })
            })
            .catch(error=>{
                console.log(error);
            })
    }

    EditLinija(){
        
          var  params={
                id: this.state.newLinija.id,
                destinacija: this.state.newLinija.destinacija,
                vremePolaska: this.state.newLinija.vremePolaska,
                cenaKarte: this.state.newLinija.cenaKarte,
                prevoznikDTO:{
                    id: this.state.newLinija.prevoznikDTO.id
                }
            }
        
        DevelopingAxios.put('/linije/' + this.props.match.params.id, params)
            .then(res=>{
                console.log(res.data)
                this.props.history.push('/linije')
            })
            .catch(error=>{
                console.log(error);
            })
    }

    onInputChange(e){
        const name = e.target.name;
        const value = e.target.value;
        let newLinija = JSON.parse(JSON.stringify(this.state.newLinija))
        newLinija[name]= value;
        this.setState({newLinija:newLinija})
    }

    onSelectPrevoznikChange(e){
        // const name = e.target.name;
        const value = e.target.value;
        console.log(value)
        let newLinija = this.state.newLinija;
        newLinija.prevoznikDTO.id = value;
        this.setState({
            newLinija:newLinija
        })
    }

    render(){
        return(
            <div>
                <Form>
                <Form.Group>
                     <Row><Col>
                    <Button variant="warning" onClick={()=>this.EditLinija()}>Edit</Button>
                    </Col></Row>
                </Form.Group>
                <Form.Group>
                    <Row><Col>
                    <Form.Label>Unesi destinaciju</Form.Label>
                    <Form.Control 
                    value={this.state.newLinija.destinacija}
                    name="destinacija"
                    as="input" 
                    type="text"
                    onChange={(e)=>this.onInputChange(e)}>
                    
                    </Form.Control>
                    </Col></Row>
                </Form.Group> 
                <Form.Group> 
                    <Row><Col>  
                    <Form.Label>Unesi vreme polaska</Form.Label>
                    <Form.Control 
                    value={this.state.newLinija.vremePolaska}
                     name="vremePolaska"
                     as="input" 
                     type="text"
                     onChange={(e)=>this.onInputChange(e)}>
                     </Form.Control>
                    </Col></Row>
                </Form.Group>
                <Row>
                <Col> 
                 
                    <Form.Label>Unesi cenu karte</Form.Label>
                    <Form.Control 
                    value={this.state.newLinija.cenaKarte}
                    name="cenaKarte"
                    as="input" 
                    type="number"
                    onChange={(e)=>this.onInputChange(e)}>
                    </Form.Control>
                <Form.Group>
                </Form.Group>  
                </Col>  
                <Col> 
                <Form.Group>
                    <Form.Label>Broj slobodnih mesta</Form.Label>
                    <Form.Control 
                    value={this.state.newLinija.brojMesta}
                    name="brojMesta"
                    as="input" 
                    type="number"
                    onChange={(e)=>this.onInputChange(e)}>
                    </Form.Control> 
                </Form.Group>
                </Col>
                </Row>
                <Row>
                    <Col>
                    <Form.Group>
                        <Form.Control
                        value={this.state.newLinija.prevoznikDTO.id}
                        name="prevoznikId"
                        as="select"
                        onChange={(e)=>this.onSelectPrevoznikChange(e)} 
                        >
                            <option value={-1}>Izaberi prevoznika</option>
                            {this.state.prevoznici.map(prevoznik=>{
                                return(
                                    <option value={prevoznik.id} key={prevoznik.id}>{prevoznik.naziv}</option>
                                );
                            })}
                        </Form.Control>
                    </Form.Group>
                    </Col>
                </Row>
                </Form>
            </div>
        );
    }

}
export default EditLinija;